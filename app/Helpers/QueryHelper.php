<?php

if (!function_exists('where')) {
    function where($query, $field, $value)
    {
        return is_array($value) ?
            $query->whereIn($field, $value) :
            $query->where($field, $value);
    }
}

if (!function_exists('where_like')) {
    function where_like($query, $field, $value)
    {
        if (is_array($value)) {
            return $query->where(function ($query) use ($field, $value) {
                foreach ($value as $key => $value) {
                    $query->orwhere($field, 'like', "%{$value}%");
                }
            });
        } else {
            return $query->where($field, 'like', "%{$value}%");
        }
    }
}

if (!function_exists('obj_arr')) {
    function obj_arr($obj)
    {
        return json_decode(json_encode($obj), true);
    }
}
