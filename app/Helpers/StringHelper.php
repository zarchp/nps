<?php

use Illuminate\Support\Str;

if (!function_exists('generate_random_code')) {
    function generate_random_code($length = 3)
    {
        return strtoupper(Str::random($length));
    }
}
