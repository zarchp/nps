<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\AddressResource;
use App\Models\Address;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = request('per_page', 100);
        $sortBy = request('sort_by', 'id');
        $orderBy = request('order_by', 'asc');
        $userId = request('user_id');
        $name = request('name');
        $address = request('address');
        $isCustomerPrimary = request('is_customer_primary');
        $isAgentPrimary = request('is_agent_primary');

        $addresses = Address::
            with([
                'area',
                'subarea'
            ])
            ->when($sortBy, function ($query, $sortBy) use ($orderBy) {
                return $query->orderBy($sortBy, $orderBy);
            })
            ->when($userId, function ($query, $userId) {
                where($query, 'user_id', $userId);
            })
            ->when($name, function ($query, $name) {
                where_like($query, 'name', $name);
            })
            ->when($address, function ($query, $address) {
                where_like($query, 'address', $address);
            })
            ->when($isCustomerPrimary === "1", function ($query, $isCustomerPrimary) {
                return $query->where('is_customer_primary', $isCustomerPrimary);
            })
            ->when($isCustomerPrimary === "0", function ($query) use ($isCustomerPrimary) {
                return $query->where('is_customer_primary', $isCustomerPrimary);
            })
            ->when($isAgentPrimary === "1", function ($query, $isAgentPrimary) {
                return $query->where('is_agent_primary', $isAgentPrimary);
            })
            ->when($isAgentPrimary === "0", function ($query) use ($isAgentPrimary) {
                return $query->where('is_agent_primary', $isAgentPrimary);
            })
            ->paginate($perPage);

        return AddressResource::collection($addresses);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $address = Address::create($request->all());

        return (new AddressResource($address))
            ->response()
            ->header('Location', route('addresses.show', $address->id))
            ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function show(Address $address)
    {
        return new AddressResource($address);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Address $address)
    {
        $address->update($request->all());

        return new AddressResource($address);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function destroy(Address $address)
    {
        $address->delete();

        return response()->json([], 204);
    }
}
