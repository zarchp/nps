<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\AreaResource;
use App\Models\Area;
use Illuminate\Http\Request;

class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = request('per_page', 100);
        $sortBy = request('sort_by', 'id');
        $orderBy = request('order_by', 'asc');
        $code = request('code');
        $name = request('name');
        $isActive = request('is_active');

        $areas = Area::
            with([
                'city',
                'city.province',
                'subareas'
            ])
            ->when($sortBy, function ($query, $sortBy) use ($orderBy) {
                return $query->orderBy($sortBy, $orderBy);
            })
            ->when($code, function ($query, $code) {
                where($query, 'code', $code);
            })
            ->when($name, function ($query, $name) {
                where_like($query, 'name', $name);
            })
            ->when($isActive === "1", function ($query, $isActive) {
                return $query->where('is_active', $isActive);
            })
            ->when($isActive === "0", function ($query) use ($isActive) {
                return $query->where('is_active', $isActive);
            })
            ->paginate($perPage);

        return AreaResource::collection($areas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $area = Area::create($request->all());

        return (new AreaResource($area))
            ->response()
            ->header('Location', route('areas.show', $area->id))
            ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function show(Area $area)
    {
        return new AreaResource($area);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Area $area)
    {
        $area->update($request->all());

        return new AreaResource($area);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function destroy(Area $area)
    {
        $area->delete();

        return response()->json([], 204);
    }
}
