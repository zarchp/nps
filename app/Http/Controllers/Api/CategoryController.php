<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = request('per_page', 100);
        $sortBy = request('sort_by', 'id');
        $orderBy = request('order_by', 'asc');
        $parentId = request('parent_id');
        $name = request('name');
        $isActive = request('is_active');

        $categories = Category::
            with([
                'parent'
            ])
            ->when($sortBy, function ($query, $sortBy) use ($orderBy) {
                return $query->orderBy($sortBy, $orderBy);
            })
            ->when($parentId, function ($query, $parentId) {
                where($query, 'parent_id', $parentId);
            })
            ->when($name, function ($query, $name) {
                where_like($query, 'name', $name);
            })
            ->when($isActive === "1", function ($query, $isActive) {
                return $query->where('is_active', $isActive);
            })
            ->when($isActive === "0", function ($query) use ($isActive) {
                return $query->where('is_active', $isActive);
            })
            ->paginate($perPage);

        return CategoryResource::collection($categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = Category::create($request->all());

        return (new CategoryResource($category))
            ->response()
            ->header('Location', route('categories.show', $category->id))
            ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return new CategoryResource($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $category->update($request->all());

        return new CategoryResource($category);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();

        return response()->json([], 204);
    }
}
