<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\MerchantResource;
use App\Models\Merchant;
use Illuminate\Http\Request;

class MerchantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = request('per_page', 100);
        $sortBy = request('sort_by', 'id');
        $orderBy = request('order_by', 'asc');
        $code = request('code');
        $name = request('name');
        $isActive = request('is_active');

        $merchants = Merchant::
            with([
                'categories'
            ])
            ->when($sortBy, function ($query, $sortBy) use ($orderBy) {
                return $query->orderBy($sortBy, $orderBy);
            })
            ->when($code, function ($query, $code) {
                where($query, 'code', $code);
            })
            ->when($name, function ($query, $name) {
                where_like($query, 'name', $name);
            })
            ->when($isActive === "1", function ($query, $isActive) {
                return $query->where('is_active', $isActive);
            })
            ->when($isActive === "0", function ($query) use ($isActive) {
                return $query->where('is_active', $isActive);
            })
            ->paginate($perPage);

        return MerchantResource::collection($merchants);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $merchant = Merchant::create($request->all());

        return (new MerchantResource($merchant))
            ->response()
            ->header('Location', route('merchants.show', $merchant->id))
            ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Merchant  $merchant
     * @return \Illuminate\Http\Response
     */
    public function show(Merchant $merchant)
    {
        $merchant->load([
            'categories'
        ]);

        return new MerchantResource($merchant);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Merchant  $merchant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Merchant $merchant)
    {
        $merchant->update($request->all());

        return new MerchantResource($merchant);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Merchant  $merchant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Merchant $merchant)
    {
        $merchant->delete();

        return response()->json([], 204);
    }
}
