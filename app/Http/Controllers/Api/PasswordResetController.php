<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PasswordResetResource;
use App\Models\PasswordReset;
use App\Models\User;
use App\Notifications\Auth\PasswordResetRequest;
use App\Notifications\Auth\PasswordResetSuccess;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PasswordResetController extends Controller
{
    public function create()
    {
        request()->validate([
            'email' => 'required|string|email'
        ]);

        $user = User::where('email', request('email'))->first();
        if (!$user) {
            return response()->json([
                'message' => 'We can\'t find a user with that email address.'
            ], 404);
        }

        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => Str::random(60)
            ]
        );

        if ($user && $passwordReset) {
            $user->notify(new PasswordResetRequest($passwordReset->token));
        }

        return response()->json([
            'message' => 'We have emailed your password reset link!'
        ], 200);
    }

    public function find($token)
    {
        $passwordReset = PasswordReset::where('token', $token)->first();
        if (!$passwordReset) {
            return response()->json([
                'message' => 'This password reset token is invalid'
            ], 404);
        }
        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();

            return response()->json([
                'message' => 'This password reset token is invalid'
            ], 404);
        }

        return new PasswordResetResource($passwordReset);
    }

    public function reset()
    {
        DB::beginTransaction();
        request()->validate([
            'email' => 'required|string|email',
            'password' => 'required|string|confirmed',
            'token' => 'required|string'
        ]);

        $passwordReset = PasswordReset::
            where([
                ['email', request('email')],
                ['token', request('token')]
            ])
            ->first();
        if (!$passwordReset) {
            return response()->json([
                'message' => 'This password reset token is invalid'
            ], 404);
        }

        $user = User::where('email', $passwordReset->email)->first();
        if (!$user) {
            return response()->json([
                'message' => 'We can\'t find a user with that email address.'
            ], 404);
        }

        $passwordReset->delete();

        $user->update([
            'password' => bcrypt(request('password'))
        ]);
        $user->notify(new PasswordResetSuccess($passwordReset));

        DB::commit();
        return response()->json([
            'message' => 'Your password has been reset successfully!'
        ], 200);
    }
}
