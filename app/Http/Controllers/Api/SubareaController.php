<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\SubareaResource;
use App\Models\Subarea;
use Illuminate\Http\Request;

class SubareaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = request('per_page', 100);
        $sortBy = request('sort_by', 'id');
        $orderBy = request('order_by', 'asc');
        $code = request('code');
        $name = request('name');
        $isActive = request('is_active');

        $subareas = Subarea::
            with([
                'area'
            ])
            ->when($sortBy, function ($query, $sortBy) use ($orderBy) {
                return $query->orderBy($sortBy, $orderBy);
            })
            ->when($code, function ($query, $code) {
                where($query, 'code', $code);
            })
            ->when($name, function ($query, $name) {
                where_like($query, 'name', $name);
            })
            ->when($isActive === "1", function ($query, $isActive) {
                return $query->where('is_active', $isActive);
            })
            ->when($isActive === "0", function ($query) use ($isActive) {
                return $query->where('is_active', $isActive);
            })
            ->paginate($perPage);

        return SubareaResource::collection($subareas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $subarea = Subarea::create($request->all());

        return (new SubareaResource($subarea))
            ->response()
            ->header('Location', route('subareas.show', $subarea->id))
            ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Subarea  $subarea
     * @return \Illuminate\Http\Response
     */
    public function show(Subarea $subarea)
    {
        return new SubareaResource($subarea);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Subarea  $subarea
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subarea $subarea)
    {
        $subarea->update($request->all());

        return new SubareaResource($subarea);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Subarea  $subarea
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subarea $subarea)
    {
        $subarea->delete();

        return response()->json([], 204);
    }
}
