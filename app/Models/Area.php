<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Area extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['city_id', 'code', 'name', 'is_active', 'latitude', 'longitude'];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function subareas()
    {
        return $this->hasMany(Subarea::class);
    }
}
