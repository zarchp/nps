<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['province_id', 'name', 'alt_name', 'latitude', 'longitude'];

    public function province()
    {
        return $this->belongsTo(Province::class);
    }
}
