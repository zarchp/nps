<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Merchant extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['code', 'name', 'is_active'];

    public function categories()
    {
        return $this->belongsToMany(Category::class)->withTimestamps();
    }
}
