<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subarea extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['area_id', 'code', 'name', 'is_active'];

    public function area()
    {
        return $this->belongsTo(Area::class);
    }
}
