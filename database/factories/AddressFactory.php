<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Address;
use App\Models\Area;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(Address::class, function (Faker $faker) {
    return [
        'user_id' => User::all()->random()->id,
        'area_id' => Area::all()->random()->id,
        'subarea_id' => null,
        'name' => $faker->streetName,
        'address' => $faker->address,
        'is_customer_primary' => false,
        'is_agent_primary' => false,
        'postal_code' => $faker->postcode,
        'latitude' => $faker->latitude,
        'longitude' => $faker->longitude
    ];
});
