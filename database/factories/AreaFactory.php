<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Area;
use App\Models\City;
use Faker\Generator as Faker;

$factory->define(Area::class, function (Faker $faker) {
    return [
        'city_id' => City::all()->random()->id,
        'code' => generate_random_code(),
        'name' => $faker->city,
        'is_active' => true,
        'latitude' => $faker->latitude,
        'longitude' => $faker->longitude
    ];
});
