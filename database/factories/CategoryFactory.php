<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    $categoryId = Category::first() ? Category::all()->random()->id : null;

    return [
        'parent_id' => $categoryId,
        'name' => $faker->firstName,
        'depth' => 0,
        'is_active' => true
    ];
});
