<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Merchant;
use Faker\Generator as Faker;

$factory->define(Merchant::class, function (Faker $faker) {
    return [
        'code' => generate_random_code(),
        'name' => $faker->firstName,
        'is_active' => true
    ];
});
