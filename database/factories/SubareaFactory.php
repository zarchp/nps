<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Area;
use App\Models\Subarea;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Subarea::class, function (Faker $faker) {
    return [
        'area_id' => Area::all()->random()->id,
        'code' => generate_random_code(),
        'name' => $faker->streetName,
        'is_active' => true
    ];
});
