<?php

// phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace

use App\Models\Address;
use Illuminate\Database\Seeder;

class AddressesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Address::class, 10)->create();
    }
}
