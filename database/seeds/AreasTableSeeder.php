<?php

// phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace

use App\Models\Area;
use Illuminate\Database\Seeder;

class AreasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Area::class, 10)->create();
    }
}
