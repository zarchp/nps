<?php

// phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory(Category::class, 10)->create();

        $food = Category::create([
            'parent_id' => null,
            'name' => 'Food',
            'depth' => 0,
            'is_active' => true
        ]);

        $beverage = Category::create([
            'parent_id' => null,
            'name' => 'Beverage',
            'depth' => 0,
            'is_active' => true
        ]);

        Category::insert([
            [
                'parent_id' => $food->id,
                'name' => 'Rice',
                'depth' => 1,
                'is_active' => true,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'parent_id' => $food->id,
                'name' => 'Chicken',
                'depth' => 1,
                'is_active' => true,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'parent_id' => $food->id,
                'name' => 'Snacks',
                'depth' => 1,
                'is_active' => true,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'parent_id' => $food->id,
                'name' => 'Salad',
                'depth' => 1,
                'is_active' => true,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'parent_id' => $food->id,
                'name' => 'Donuts',
                'depth' => 1,
                'is_active' => true,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'parent_id' => $food->id,
                'name' => 'Martabak',
                'depth' => 1,
                'is_active' => true,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'parent_id' => $beverage->id,
                'name' => 'Coffee',
                'depth' => 1,
                'is_active' => true,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'parent_id' => $beverage->id,
                'name' => 'Tea',
                'depth' => 1,
                'is_active' => true,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'parent_id' => $beverage->id,
                'name' => 'Juice',
                'depth' => 1,
                'is_active' => true,
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);
    }
}
