<?php

// phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // region
        $this->call(ProvincesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);

        // no foreign key
        $this->call(UsersTableSeeder::class);
        $this->call(AreasTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(MerchantsTableSeeder::class);

        // has foreign key
        $this->call(SubareasTableSeeder::class);
        $this->call(AddressesTableSeeder::class);
    }
}
