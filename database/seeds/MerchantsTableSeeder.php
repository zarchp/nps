<?php

// phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace

use App\Models\Category;
use App\Models\Merchant;
use Illuminate\Database\Seeder;

class MerchantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Merchant::class, 10)->create();

        $merchants = Merchant::all();
        foreach ($merchants as $merchant) {
            $categoryId = Category::all()->random()->id;
            $merchant->categories()->attach($categoryId);
        }
    }
}
