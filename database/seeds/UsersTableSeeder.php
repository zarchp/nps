<?php

// phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Anzar Syahid',
            'email' => 'anzar@example.com',
            'password' => bcrypt('123qweasd'),
            'email_verified_at' => now(),
            'is_active' => true,
            'activation_token' => Str::random(60)
        ]);
    }
}
