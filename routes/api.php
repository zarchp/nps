<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function () {
    Route::group(['prefix' => 'auth', 'name' => 'auth.'], function () {
        Route::post('signup', 'AuthController@signup')->name('signup');
        Route::get('signup/activate/{token}', 'AuthController@signupActivate')->name('signup_activate');
        Route::post('login', 'AuthController@login')->name('login');

        Route::group(['prefix' => 'password', 'name' => 'password.', 'middleware' => 'api'], function () {
            Route::post('create', 'PasswordResetController@create')->name('create');
            Route::get('find/{token}', 'PasswordResetController@find')->name('find');
            Route::post('reset', 'PasswordResetController@reset')->name('reset');
        });

        Route::group(['middleware' => 'auth:api'], function () {
            Route::post('logout', 'AuthController@logout')->name('logout');
            Route::get('user', 'AuthController@user')->name('user');
        });
    });

    Route::group(['middleware' => 'auth:api'], function () {
        Route::apiResource('users', 'UserController')->except(['store']);

        Route::apiResource('areas', 'AreaController');

        Route::apiResource('subareas', 'SubareaController');

        Route::apiResource('addresses', 'AddressController');

        Route::apiResource('categories', 'CategoryController');

        Route::apiResource('merchants', 'MerchantController');
    });
});
