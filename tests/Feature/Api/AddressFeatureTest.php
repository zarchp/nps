<?php

namespace Tests\Feature\Api;

use App\Models\Address;
use App\Models\Area;
use App\Models\User;
use Tests\TestCase;

class AddressFeatureTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->id = Address::latest()->first()->id;
        $this->structure = [
            'data' => [
                'id',
                'user_id',
                'area_id',
                'subarea_id',
                'name',
                'address',
                'is_customer_primary',
                'is_agent_primary',
                'postal_code',
                'latitude',
                'longitude',
                'created_at',
                'updated_at'
            ]
        ];
        $this->structureCollection = [
            'data' => [$this->structure['data']]
        ];
        $this->data = [
            'user_id' => User::all()->random()->id,
            'area_id' => Area::all()->random()->id,
            'subarea_id' => null,
            'name' => $this->faker->streetName,
            'address' => $this->faker->address,
            'is_customer_primary' => false,
            'is_agent_primary' => false,
            'postal_code' => $this->faker->postcode,
            'latitude' => $this->faker->latitude,
            'longitude' => $this->faker->longitude
        ];
    }

    public function testItCanGetAddresses()
    {
        $response = $this->actingAs($this->user, 'api')
            ->getJson('/api/addresses');

        $response->assertStatus(200)
            ->assertJsonStructure($this->structureCollection);
    }

    public function testItCanCreateAddress()
    {
        $response = $this->actingAs($this->user, 'api')
            ->postJson('/api/addresses', $this->data);

        $response->assertStatus(201);
    }

    public function testItCanGetAddress()
    {
        $response = $this->actingAs($this->user, 'api')
            ->getJson('/api/addresses/' . $this->id);

        $response->assertStatus(200)
            ->assertJsonStructure($this->structure);
    }

    public function testItCanUpdateAddress()
    {
        $response = $this->actingAs($this->user, 'api')
            ->putJson('/api/addresses/' . $this->id, $this->data);

        $response->assertStatus(200)
            ->assertJsonStructure($this->structure);
    }

    public function testItCanDeleteAddress()
    {
        $response = $this->actingAs($this->user, 'api')
            ->deleteJson('/api/addresses/' . $this->id);

        $response->assertStatus(204);
    }
}
