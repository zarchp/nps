<?php

namespace Tests\Feature\Api;

use App\Models\Area;
use App\Models\City;
use Tests\TestCase;

class AreaFeatureTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->id = Area::latest()->first()->id;
        $this->structure = [
            'data' => [
                'id',
                'city_id',
                'code',
                'name',
                'is_active',
                'latitude',
                'longitude',
                'created_at',
                'updated_at'
            ]
        ];
        $this->structureCollection = [
            'data' => [$this->structure['data']]
        ];
        $this->data = [
            'city_id' => City::all()->random()->id,
            'code' => generate_random_code(),
            'name' => $this->faker->city,
            'is_active' => true,
            'latitude' => $this->faker->latitude,
            'longitude' => $this->faker->longitude
        ];
    }

    public function testItCanGetAreas()
    {
        $response = $this->actingAs($this->user, 'api')
            ->getJson('/api/areas');

        $response->assertStatus(200)
            ->assertJsonStructure($this->structureCollection);
    }

    public function testItCanCreateArea()
    {
        $response = $this->actingAs($this->user, 'api')
            ->postJson('/api/areas', $this->data);

        $response->assertStatus(201);
    }

    public function testItCanGetArea()
    {
        $response = $this->actingAs($this->user, 'api')
            ->getJson('/api/areas/' . $this->id);

        $response->assertStatus(200)
            ->assertJsonStructure($this->structure);
    }

    public function testItCanUpdateArea()
    {
        $response = $this->actingAs($this->user, 'api')
            ->putJson('/api/areas/' . $this->id, $this->data);

        $response->assertStatus(200)
            ->assertJsonStructure($this->structure);
    }

    public function testItCanDeleteArea()
    {
        $response = $this->actingAs($this->user, 'api')
            ->deleteJson('/api/areas/' . $this->id);

        $response->assertStatus(204);
    }
}
