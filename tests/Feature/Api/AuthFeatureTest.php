<?php

namespace Tests\Feature\Api;

use Tests\TestCase;

class AuthFeatureTest extends TestCase
{
    public function testItCanSignup()
    {
        $response = $this
            ->postJson('/api/auth/signup', [
                'name' => $this->faker->name,
                'email' => $this->faker->unique()->safeEmail,
                'password' => '123qweasd',
                'password_confirmation' => '123qweasd'
            ]);

        $response->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'email',
                    'name'
                ]
            ]);
    }

    public function testItErrorWhenLoginFailed()
    {
        $response = $this
            ->postJson('/api/auth/login', [
                'email' => 'hello@example.com',
                'password' => '123qwe'
            ]);

        $response->assertStatus(401)
            ->assertJsonStructure([
                'message'
            ]);
    }

    public function testItCanLogin()
    {
        $response = $this
            ->postJson('/api/auth/login', [
                'email' => $this->user->email,
                'password' => '123qweasd',
                'remember_me' => true
            ]);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'access_token',
                    'token_type',
                    'expires_at'
                ]
            ]);
    }

    public function testItCanLogout()
    {
        // login first
        $auth = $this
            ->postJson('/api/auth/login', [
                'email' => $this->user->email,
                'password' => '123qweasd'
            ])
            ->decodeResponseJson();

        $response = $this->withHeader('Authorization', 'Bearer ' . $auth['data']['access_token'])
            ->postJson('/api/auth/logout');

        $response->assertStatus(200)
            ->assertJsonStructure([
                'message'
            ]);
    }

    public function testItCanGetProfile()
    {
        $this->actingAs($this->user, 'api')
            ->getJson('/api/auth/user')
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name',
                    'email',
                    'email_verified_at',
                    'photo',
                    'is_active',
                    'activation_token',
                    'created_at'
                ]
            ]);
    }
}
