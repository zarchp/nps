<?php

namespace Tests\Feature\Api;

use App\Models\Category;
use Tests\TestCase;

class CategoryFeatureTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->id = Category::latest()->first()->id;
        $this->structure = [
            'data' => [
                'id',
                'parent_id',
                'name',
                'depth',
                'is_active',
                'created_at',
                'updated_at'
            ]
        ];
        $this->structureCollection = [
            'data' => [$this->structure['data']]
        ];
        $this->data = [
            'parent_id' => null,
            'name' => $this->faker->firstName,
            'depth' => 0,
            'is_active' => true
        ];
    }

    public function testItCanGetCategories()
    {
        $response = $this->actingAs($this->user, 'api')
            ->getJson('/api/categories');

        $response->assertStatus(200)
            ->assertJsonStructure($this->structureCollection);
    }

    public function testItCanCreateCategory()
    {
        $response = $this->actingAs($this->user, 'api')
            ->postJson('/api/categories', $this->data);

        $response->assertStatus(201);
    }

    public function testItCanGetCategory()
    {
        $response = $this->actingAs($this->user, 'api')
            ->getJson('/api/categories/' . $this->id);

        $response->assertStatus(200)
            ->assertJsonStructure($this->structure);
    }

    public function testItCanUpdateCategory()
    {
        $response = $this->actingAs($this->user, 'api')
            ->putJson('/api/categories/' . $this->id, $this->data);

        $response->assertStatus(200)
            ->assertJsonStructure($this->structure);
    }

    public function testItCanDeleteCategory()
    {
        $response = $this->actingAs($this->user, 'api')
            ->deleteJson('/api/categories/' . $this->id);

        $response->assertStatus(204);
    }
}
