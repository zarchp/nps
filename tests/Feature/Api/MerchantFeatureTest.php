<?php

namespace Tests\Feature\Api;

use App\Models\Merchant;
use Tests\TestCase;

class MerchantFeatureTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->id = Merchant::latest()->first()->id;
        $this->structure = [
            'data' => [
                'id',
                'code',
                'name',
                'is_active',
                'created_at',
                'updated_at'
            ]
        ];
        $this->structureCollection = [
            'data' => [$this->structure['data']]
        ];
        $this->data = [
            'code' => generate_random_code(),
            'name' => $this->faker->firstName,
            'is_active' => true
        ];
    }

    public function testItCanGetMerchants()
    {
        $response = $this->actingAs($this->user, 'api')
            ->getJson('/api/merchants');

        $response->assertStatus(200)
            ->assertJsonStructure($this->structureCollection);
    }

    public function testItCanCreateMerchant()
    {
        $response = $this->actingAs($this->user, 'api')
            ->postJson('/api/merchants', $this->data);

        $response->assertStatus(201);
    }

    public function testItCanGetMerchant()
    {
        $response = $this->actingAs($this->user, 'api')
            ->getJson('/api/merchants/' . $this->id);

        $response->assertStatus(200)
            ->assertJsonStructure($this->structure);
    }

    public function testItCanUpdateMerchant()
    {
        $response = $this->actingAs($this->user, 'api')
            ->putJson('/api/merchants/' . $this->id, $this->data);

        $response->assertStatus(200)
            ->assertJsonStructure($this->structure);
    }

    public function testItCanDeleteMerchant()
    {
        $response = $this->actingAs($this->user, 'api')
            ->deleteJson('/api/merchants/' . $this->id);

        $response->assertStatus(204);
    }
}
