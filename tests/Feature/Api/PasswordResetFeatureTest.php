<?php

namespace Tests\Feature\Api;

use App\Models\PasswordReset;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\TestCase;

class PasswordResetFeatureTest extends TestCase
{
    public function testCreate()
    {
        $response = $this->actingAs($this->user, 'api')
            ->postJson('/api/auth/password/create', [
                'email' => $this->user->email
            ]);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'message'
            ]);
    }

    public function testFind()
    {
        $token = Str::random(60);
        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $this->user->email],
            [
                'email' => $this->user->email,
                'token' => $token
            ]
        );

        $response = $this->actingAs($this->user, 'api')
            ->getJson('/api/auth/password/find/' . $token);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'email',
                    'token'
                ]
            ]);
    }

    public function testReset()
    {
        $token = Str::random(60);
        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $this->user->email],
            [
                'email' => $this->user->email,
                'token' => $token
            ]
        );

        $response = $this->actingAs($this->user, 'api')
            ->postJson('/api/auth/password/reset', [
                'email' => $this->user->email,
                'token' => $token,
                'password' => '123qweasd',
                'password_confirmation' => '123qweasd'
            ]);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'message'
            ]);
    }
}
