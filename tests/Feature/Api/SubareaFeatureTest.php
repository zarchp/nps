<?php

namespace Tests\Feature\Api;

use App\Models\Area;
use App\Models\Subarea;
use Tests\TestCase;

class SubaFeatureTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->id = Subarea::latest()->first()->id;
        $this->structure = [
            'data' => [
                'id',
                'area_id',
                'code',
                'name',
                'is_active',
                'created_at',
                'updated_at'
            ]
        ];
        $this->structureCollection = [
            'data' => [$this->structure['data']]
        ];
        $this->data = [
            'area_id' => Area::all()->random()->id,
            'code' => generate_random_code(),
            'name' => $this->faker->streetName,
            'is_active' => true
        ];
    }

    public function testItCanGetSubareas()
    {
        $response = $this->actingAs($this->user, 'api')
            ->getJson('/api/subareas');

        $response->assertStatus(200)
            ->assertJsonStructure($this->structureCollection);
    }

    public function testItCanCreateSubarea()
    {
        $response = $this->actingAs($this->user, 'api')
            ->postJson('/api/subareas', $this->data);

        $response->assertStatus(201);
    }

    public function testItCanGetSubarea()
    {
        $response = $this->actingAs($this->user, 'api')
            ->getJson('/api/subareas/' . $this->id);

        $response->assertStatus(200)
            ->assertJsonStructure($this->structure);
    }

    public function testItCanUpdateSubarea()
    {
        $response = $this->actingAs($this->user, 'api')
            ->putJson('/api/subareas/' . $this->id, $this->data);

        $response->assertStatus(200)
            ->assertJsonStructure($this->structure);
    }

    public function testItCanDeleteSubarea()
    {
        $response = $this->actingAs($this->user, 'api')
            ->deleteJson('/api/subareas/' . $this->id);

        $response->assertStatus(204);
    }
}
