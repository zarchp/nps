<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Str;

class UserFeatureTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->id = User::latest()->first()->id;
        $this->structure = [
            'data' => [
                'id',
                'name',
                'email',
                'email_verified_at',
                'photo',
                'photo_url',
                'is_active',
                'activation_token',
                'created_at',
                'updated_at'
            ]
        ];
        $this->structureCollection = [
            'data' => [$this->structure['data']]
        ];
        $this->data = [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->email,
            'email_verified_at' => now(),
            'is_active' => true,
            'activation_token' => Str::random(60)
        ];
    }

    public function testItCanGetUsers()
    {
        $response = $this->actingAs($this->user, 'api')
            ->getJson('/api/users');

        $response->assertStatus(200)
            ->assertJsonStructure($this->structureCollection);
    }

    public function testItCanGetUser()
    {
        $response = $this->actingAs($this->user, 'api')
            ->getJson('/api/users/' . $this->id);

        $response->assertStatus(200)
            ->assertJsonStructure($this->structure);
    }

    public function testItCanUpdateUser()
    {
        $response = $this->actingAs($this->user, 'api')
            ->putJson('/api/users/' . $this->id, $this->data);

        $response->assertStatus(200)
            ->assertJsonStructure($this->structure);
    }

    public function testItCanDeleteUser()
    {
        $response = $this->actingAs($this->user, 'api')
            ->deleteJson('/api/users/' . $this->id);

        $response->assertStatus(204);
    }
}
