<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\WithFaker;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, WithFaker;

    protected $faker;

    protected $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->faker('id_ID');
        // $this->artisan('migrate:fresh --seed');
        // $this->artisan('passport:install --force');
        $this->user = User::where('email', 'anzar@example.com')->first();
    }
}
