<?php

namespace Tests\Unit;

use Tests\TestCase;

class ScheduleTest extends TestCase
{
    public function testSchedule()
    {
        $this->artisan('schedule:run')
            ->assertExitCode(0);
    }
}
